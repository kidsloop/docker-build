FROM registry.lab.local/devops/alpine:3.11.6
MAINTAINER Cao Le Thanh (leethanh21121986@gmail.com)
ENV ANSIBLE_VERSION 2.9.10
ENV JINJA2_VERSION 2.11.1
ENV PBR_VERSION 5.4.4
ENV HVAC_VERSION 0.10.0
ENV JMESPATH_VERSION 0.9.5
ENV RUAMEL_VERSION 0.16.10
ENV NETADDR_VERSION 0.7.19

RUN apk update && apk upgrade
RUN apk add ca-certificates && update-ca-certificates
RUN apk add --no-cache --update \
        curl \
        unzip \
        bash \
        python2 \
        libffi-dev \
        python2-dev \
        py-pip \
        jq \
        openssh-client \
        gettext \
        git \
        make \
        build-base \
        openssl-dev \        
        sshpass \
        openssh-client \
        openjdk8 \
        tzdata
#RUN if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
#    echo "**** install pip ****" && \
#    python3 -m ensurepip && \
#    rm -r /usr/lib/python*/ensurepip && \
#    pip3 install --no-cache --upgrade pip setuptools wheel requests jmespath && \
#    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi



# Install Ansible Requirement
RUN echo "===> Installing Ansible..." && \
    pip install ansible==${ANSIBLE_VERSION}
RUN echo "===> Installing JINJA2..." && \
    pip install Jinja2
RUN echo "===> Installing PBR..." && \
    pip install PBR==${PBR_VERSION}
RUN echo "===> Installing hvac..." && \
    pip install hvac==${HVAC_VERSION}	
RUN echo "===> Installing jmespath..." && \
    pip install jmespath==${JMESPATH_VERSION}	
RUN echo "===> Installing ruamel.yaml..." && \
    pip install ruamel.yaml==${RUAMEL_VERSION}	
RUN echo "===> Installing netaddr..." && \
    pip install netaddr==${NETADDR_VERSION}	
RUN echo "===> Installing aws-cli..." && \
    pip install awscli
RUN echo "===> Installing boto..." && \
    pip install boto
RUN echo "===> Installing botocore..." && \
    pip install botocore 
RUN echo "===> Installing boto3..." && \
    pip install boto3
RUN echo "===> Installing docker-py..." && \
    pip install docker

# Install RKE
COPY rke /usr/local/bin/rke
RUN chmod 700 /usr/local/bin/rke
RUN rke --version

# Install Kubectl
COPY kubectl /usr/local/bin/kubectl
RUN chmod 700 /usr/local/bin/kubectl

# Install helm
COPY helm /usr/local/bin/helm
RUN chmod 700 /usr/local/bin/helm
RUN helm version
